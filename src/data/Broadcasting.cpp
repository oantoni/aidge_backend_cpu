/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/data/Broadcasting.hpp"

std::vector<std::size_t> Aidge::getBroadcastedDims(const std::vector<std::size_t>& outputDims, const std::vector<std::size_t>& dimsToBroadcast){
    std::vector<std::size_t> broadcastedDims(outputDims.size(), 1);
		for(int j=dimsToBroadcast.size()-1; j>=0; --j)
		{
			std::size_t idx = outputDims.size() - (dimsToBroadcast.size()-j);
			broadcastedDims[idx] = dimsToBroadcast[j];
		}
    return broadcastedDims;
}

std::vector<std::size_t> Aidge::getMultiDimIndices(const std::vector<std::size_t>& dimensions, std::size_t idx){
    std::vector<std::size_t> indices(dimensions.size(), 0);

    for (int i = dimensions.size() - 1; i >= 0; --i) {
        indices[i] = idx % dimensions[i];
        idx /= dimensions[i];
    }

    return indices;
}

std::size_t Aidge::getFlattenedIndex(const std::vector<std::size_t>& dimensions, const std::vector<std::size_t>& indices){
    std::size_t flattenedIdx = 0;
    std::size_t stride = 1;

    for (int i = dimensions.size() - 1; i >= 0; --i) {
        std::size_t idx = dimensions[i]>1 ? indices[i] : 0;
        flattenedIdx += idx * stride;
        stride *= dimensions[i];
    }
    return flattenedIdx;
}

