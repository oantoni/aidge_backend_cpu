/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <numeric> // std::accumulate
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/operator/BatchNorm.hpp"

#include "aidge/backend/cpu/operator/BatchNormImpl.hpp"
#include "aidge/backend/cpu/operator/BatchNormImpl_forward_kernels.hpp"

Aidge::Elts_t Aidge::BatchNormImpl2D_cpu::getNbRequiredProtected(IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::BatchNormImpl2D_cpu::forward() {
    assert(mOp.getRawInput(0) && "missing input #0");
    assert(mOp.getRawInput(1) && "missing input #1");
    assert(mOp.getRawInput(2) && "missing input #2");
    assert(mOp.getRawInput(3) && "missing input #3");
    assert(mOp.getRawInput(4) && "missing input #4");

    assert(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->nbDims() == 4);
    // Find the correct kernel type
    auto kernelFunc =
            Registrar<BatchNormImpl2DForward_cpu>::create({std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dataType(),
                                                           std::static_pointer_cast<Tensor>(mOp.getRawInput(1))->dataType(),
                                                           std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()});

    // Call kernel
    kernelFunc(dynamic_cast<const BatchNorm_Op<2>&>(mOp).getStaticAttributes(),
               std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->template dims<4>(),
               getCPUPtr(mOp.getRawInput(0)),
               getCPUPtr(mOp.getRawInput(1)),
               getCPUPtr(mOp.getRawInput(2)),
               getCPUPtr(mOp.getRawInput(3)),
               getCPUPtr(mOp.getRawInput(4)),
               getCPUPtr(mOp.getRawOutput(0)),
               true);
}
