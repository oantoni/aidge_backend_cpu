/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Sqrt.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cpu/operator/SqrtImpl.hpp"
#include "aidge/backend/cpu/operator/SqrtImpl_forward_kernels.hpp"
#include "aidge/backend/cpu/operator/SqrtImpl_backward_kernels.hpp"

Aidge::Elts_t Aidge::SqrtImpl_cpu::getNbRequiredProtected(const Aidge::IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::SqrtImpl_cpu::forward() {
    std::shared_ptr<Tensor> in0 = std::static_pointer_cast<Tensor>(mOp.getRawInput(0));
    std::shared_ptr<Tensor> out0 = std::static_pointer_cast<Tensor>(mOp.getRawOutput(0));
    AIDGE_ASSERT(in0, "missing input #0");

    // Find the correct kernel type
    auto kernelFunc = Registrar<SqrtImplForward_cpu>::create({
        in0->dataType(),
        out0->dataType()});

    // Call kernel
    kernelFunc(in0->size(),
        getCPUPtr(mOp.getRawInput(0)),
        getCPUPtr(mOp.getRawOutput(0)));
}

void Aidge::SqrtImpl_cpu::backward() {
    // reversing in and out Data for backprop
    const Sqrt_Op& op_ = dynamic_cast<const Sqrt_Op&>(mOp);
    std::shared_ptr<Tensor> out0grad  = op_.getOutput(0)->grad();
    std::shared_ptr<Tensor> in0grad = op_.getInput(0)->grad();
    AIDGE_ASSERT(out0grad, "missing output #0");

    // Find the correct kernel type
    auto kernelFunc = Registrar<SqrtImplForward_cpu>::create({
        out0grad->dataType(),
        in0grad->dataType()});

    // Call kernel
    kernelFunc(out0grad->size(),
        getCPUPtr(out0grad),
        getCPUPtr(in0grad));
}