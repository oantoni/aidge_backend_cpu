/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/ErfImpl.hpp"

#include <memory>
#include <vector>

#include "aidge/backend/cpu/operator/ErfImpl_forward_kernels.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Erf.hpp"
#include "aidge/utils/Types.h"

Aidge::Elts_t Aidge::ErfImpl_cpu::getNbRequiredProtected(const Aidge::IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::ErfImpl_cpu::forward() {
    const Erf_Op& op = static_cast<const Erf_Op&>(mOp);

    // Find the correct kernel type
    auto kernelFunc = Registrar<ErfImplForward_cpu>::create({
                            op.getInput(0)->dataType(),
                            op.getOutput(0)->dataType()
                        });

    // Call kernel
    kernelFunc(
        op.getInput(0)->size(),
        op.getInput(0)->getImpl()->rawPtr(),
        op.getOutput(0)->getImpl()->rawPtr()
    );
}
