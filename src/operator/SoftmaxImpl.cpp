/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/operator/Softmax.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/SoftmaxImpl.hpp"
#include "aidge/backend/cpu/operator/SoftmaxImpl_forward_kernels.hpp"

Aidge::Elts_t Aidge::SoftmaxImpl_cpu::getNbRequiredProtected(const Aidge::IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::SoftmaxImpl_cpu::forward() {
    assert(std::static_pointer_cast<Tensor>(mOp.getRawInput(0)) && "missing input #0");
    assert(std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->nbDims()>1);

    // Find the correct kernel type
    auto kernelFunc = Registrar<SoftmaxImplForward_cpu>::create({
        std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dataType(),
        std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()});

    Softmax_Op::Attrs attr = dynamic_cast<const Softmax_Op&>(mOp).getStaticAttributes();
    const int& axisIdx = static_cast<const int&>(std::get<0>(attr));

    // Call kernel
    kernelFunc(axisIdx,
               std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dims(),
               std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->getImpl()->rawPtr(),
               std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());
}
