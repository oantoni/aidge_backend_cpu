/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/operator/Conv.hpp"

#include "aidge/backend/cpu/operator/PadImpl.hpp"
#include "aidge/backend/cpu/operator/PadImpl_forward_kernels.hpp"

Aidge::Elts_t Aidge::PadImpl2D_cpu::getNbRequiredProtected(IOIndex_t inputIdx) const {
    assert(inputIdx == 0 && "operator has only one input");
    (void) inputIdx;

    // Padding cannot be in-place!
    // We must ensure that we do not override data that has not been consummed yet.
    const auto inputSize = std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->size();
    const auto outputSize = std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->size();
    return Elts_t::DataElts(outputSize - inputSize);
}

void Aidge::PadImpl2D_cpu::forward() {
    assert(std::static_pointer_cast<Tensor>(mOp.getRawInput(0)) && "missing input #0");

    // Find the correct kernel type
    auto kernelFunc =
            Registrar<PadImpl2DForward_cpu>::create({std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dataType(), std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()});

    // Call kernel
    kernelFunc(dynamic_cast<const Pad_Op<2>&>(mOp).getStaticAttributes(),
                        std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->template dims<4>(),
                        getCPUPtr(mOp.getRawInput(0)),
                        getCPUPtr(mOp.getRawOutput(0)));
}
