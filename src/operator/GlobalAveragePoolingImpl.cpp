/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/GlobalAveragePoolingImpl.hpp"

#include <functional>
#include <memory>
#include <vector>

#include "aidge/backend/cpu/operator/GlobalAveragePoolingImpl_forward_kernels.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/GlobalAveragePooling.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"


void Aidge::GlobalAveragePoolingImpl_cpu::forward()
{
    const GlobalAveragePooling_Op& op_ = static_cast<const GlobalAveragePooling_Op&>(mOp);
    // Check if input is provided
    AIDGE_ASSERT(op_.getInput(0), "missing input 0");

    // Create the forward kernal with the wanted types
    auto kernelFunc = Registrar<GlobalAveragePoolingImplForward_cpu>::create({op_.getInput(0)->dataType(),
                                                                              op_.getOutput(0)->dataType()});

    // Call kernel
    kernelFunc(op_.getInput(0)->dims(),
               op_.getInput(0)->getImpl()->rawPtr(),
               op_.getOutput(0)->getImpl()->rawPtr());
}