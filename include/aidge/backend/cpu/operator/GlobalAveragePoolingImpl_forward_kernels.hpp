/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_FORWARD_KERNEL_H_

#include <cstddef>
#include <functional>  // std::multiplies
#include <numeric>     // std::accumulate
#include <vector>

#include "aidge/backend/cpu/operator/GlobalAveragePoolingImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"


namespace Aidge {
template <class I, class O>
void GlobalAveragePoolingImpl_cpu_forward_kernel(
    const std::vector<DimSize_t> &dims, const void *input_, void *output_) {
  // error checking
    AIDGE_ASSERT(dims.size() >= 3,"GlobalAveragePool needs at least a 3 dimensions "
                 "input, number of input dim : {}",
                 dims.size());

  // computation
  const I *input = static_cast<const I *>(input_);
  O *output = static_cast<O *>(output_);

  DimSize_t nb_elems = std::accumulate(dims.begin(), dims.end(), std::size_t(1),
                                       std::multiplies<std::size_t>());

  const DimSize_t in_batch_nb_elems{nb_elems / dims[0]};
  const DimSize_t in_channel_nb_elems{in_batch_nb_elems / dims[1]};
  const DimSize_t out_batch_nb_elems{dims[1]};
  // parse channel by channel and fill each output with the average of the
  // values in the channel
  for (DimSize_t batch = 0; batch < dims[0]; ++batch) {
    for (DimSize_t channel = 0; channel < dims[1]; ++channel) {
      const I *filter_start = std::next(
          input, (batch * in_batch_nb_elems) + (channel * in_channel_nb_elems));
      I mean = 0;
      for (size_t i = 0; i < in_channel_nb_elems; ++i) {
        // Single pass numerically stable mean, using the fmaf
        mean = fmaf(filter_start[i] - mean, 1.0f/(i+1), mean);
      }
      output[batch * out_batch_nb_elems + channel] = mean;
    }
  }
}

// Then we add the Registrar declaration for different input/output types
namespace {
static Registrar<GlobalAveragePoolingImplForward_cpu>
    registrarGlobalAveragePoolingImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32},
        Aidge::GlobalAveragePoolingImpl_cpu_forward_kernel<float, float>);
static Registrar<GlobalAveragePoolingImplForward_cpu>
    registrarGlobalAveragePoolingImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32},
        Aidge::GlobalAveragePoolingImpl_cpu_forward_kernel<int, int>);
static Registrar<GlobalAveragePoolingImplForward_cpu>
    registrarGlobalAveragePoolingImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64},
        Aidge::GlobalAveragePoolingImpl_cpu_forward_kernel<double, double>);
} // namespace
} // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_FORWARD_KERNEL_H_ */
