/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_H_
#define AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/GlobalAveragePooling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// class GlobalAveragePooling_Op;

class GlobalAveragePoolingImplForward_cpu
    : public Registrable<
          GlobalAveragePoolingImplForward_cpu, std::tuple<DataType, DataType>,
          void(const std::vector<DimSize_t> &, const void *, void *)> {};

class GlobalAveragePoolingImplBackward_cpu
    : public Registrable<
          GlobalAveragePoolingImplBackward_cpu, std::tuple<DataType, DataType>,
          void(const std::vector<DimSize_t> &, const void *, void *)> {};

class GlobalAveragePoolingImpl_cpu : public OperatorImpl {
public:
  GlobalAveragePoolingImpl_cpu(const GlobalAveragePooling_Op &op)
      : OperatorImpl(op, "cpu") {}

  static std::unique_ptr<GlobalAveragePoolingImpl_cpu>
  create(const GlobalAveragePooling_Op &op) {
    return std::make_unique<GlobalAveragePoolingImpl_cpu>(op);
  }

  void forward() override;
};

namespace {
static Registrar<GlobalAveragePooling_Op> registrarGlobalAveragePoolingImpl_cpu(
    "cpu", Aidge::GlobalAveragePoolingImpl_cpu::create);
}
} // namespace Aidge

#endif /* _AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_H_ */
