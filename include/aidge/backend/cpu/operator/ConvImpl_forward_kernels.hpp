/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CONVIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_CONVIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/data/half.hpp"
#include "aidge/backend/cpu/operator/ConvImpl.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <cmath>
#include <array>
#include <algorithm>

namespace Aidge {
/**
 * @brief Forward kernel for 2D Convolution on CPU backend.
 * @tparam I Input data type.
 * @tparam W Weight data type.
 * @tparam B Bias data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param weights_ const weight Tensor.
 * @param biases_ const Biais Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class W, class B, class O>
void ConvImpl2D_cpu_forward_kernel(const Conv_Op<2>::Attrs &attrs, const std::array<DimSize_t, 4> &dims,
                                       const void *input_, const void *weights_, const void *biases_, void *output_) {
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    const W *weights = static_cast<const W *>(weights_);
    const B *biases = static_cast<const B *>(biases_);
    O *output = static_cast<O *>(output_);
/*
    // output H size
    const std::size_t oxSize =
            static_cast<std::size_t>(static_cast<float>(dims[0] - std::get<4>(attrs)[0] + std::get<0>(attrs)[0]) /
                                static_cast<float>(std::get<0>(attrs)[0]));
    // output W size
    const std::size_t oySize =
            static_cast<std::size_t>(static_cast<float>(dims[1] - std::get<4>(attrs)[1] + std::get<0>(attrs)[1]) /
                                static_cast<float>(std::get<0>(attrs)[1]));

    // TODO: kernel computation
    // output (Xout, Yout, outCh, batch)
    // input  (Xin, Yin, inCh, batch)
    // weight (kernelX, kernelY, inCh, outCh)
    // does not take Dilation attribute into account
    for (std::size_t ox = 0; ox < oxSize; ++ox) {
        for (std::size_t oy = 0; oy < oySize; ++oy) {
            const std::size_t ix = ox * std::get<0>(attrs)[0];
            const std::size_t iy = oy * std::get<0>(attrs)[1];

            for (std::size_t outCh = 0; outCh < std::get<3>(attrs); ++outCh) {
                const std::size_t oIndex = dims[3] * (outCh + std::get<3>(attrs) * (oy + oySize * ox));
                B biasVal = (biases != nullptr) ? biases[outCh] : B(0);
                for (std::size_t batch = 0; batch < dims[3]; ++batch) {
                    output[oIndex + batch] = biasVal;
                }
                for (std::size_t inCh = 0; inCh < dims[2]; ++inCh) {
                    for (std::size_t sx = 0; sx < std::get<4>(attrs)[0]; ++sx) {
                        for (std::size_t sy = 0; sy < std::get<4>(attrs)[1]; ++sy) {
                            const std::size_t wIndex =
                                    outCh + std::get<3>(attrs) * (inCh + dims[2] * (sy + std::get<4>(attrs)[1] * sx));
                            std::size_t iIndex = dims[3] * (inCh + dims[2] * ((iy + sy) + dims[1] * (ix + sx)));
                            for (std::size_t batch = 0; batch < dims[3]; ++batch) {
                                output[oIndex + batch] += weights[wIndex] * input[iIndex + batch];
                            }
                        }
                    }
                }
            }
        }
    }
*/


    // output H size
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[2] - std::get<4>(attrs)[0] + std::get<0>(attrs)[0]) /
                                static_cast<float>(std::get<0>(attrs)[0])));
    // output W size
    const std::size_t oySize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[3] - std::get<4>(attrs)[1] + std::get<0>(attrs)[1]) /
                                static_cast<float>(std::get<0>(attrs)[1])));

    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, inCh, Xin, Yin)
    // weight (outCh, inCh, kernelX, kernelY)
    // does not take Dilation attribute into account
    using signedsize = std::make_signed<std::size_t>::type;
    for (std::size_t batch = 0; batch < dims[0]; ++batch) {
        for (std::size_t outCh = 0; outCh < std::get<3>(attrs); ++outCh) {
            const std::size_t oIndex = (outCh + batch*std::get<3>(attrs)) * oxSize * oySize;
            // If  NoBias or bias = nullptr, set B(0)
            B biasVal = ((!std::get<5>(attrs)) && biases != nullptr) ? biases[outCh] : B(0);
            std::fill(output + oIndex, output+(oIndex+oxSize*oySize), biasVal);
            for (std::size_t inCh = 0; inCh < dims[1]; ++inCh) {
                const std::size_t iIndex = (inCh + batch*dims[1]) * dims[2] * dims[3];
                const std::size_t wIndex = (inCh + outCh*dims[1]) * std::get<4>(attrs)[0] * std::get<4>(attrs)[1];
                for (std::size_t ox = 0; ox < oxSize; ++ox) {
                    const signedsize difx = static_cast<signedsize>(- ox * std::get<0>(attrs)[0]);
                    const std::size_t sxMin = static_cast<std::size_t>(std::max(difx, signedsize(0)));
                    const std::size_t sxMax = (static_cast<signedsize>(dims[2]) + difx) < 0 ? 0 : ((dims[2] + difx) > std::get<4>(attrs)[0] ? std::get<4>(attrs)[0] : dims[2] + difx);
                    for (std::size_t oy = 0; oy < oySize; ++oy) {
                        const signedsize dify = static_cast<signedsize>(- oy * std::get<0>(attrs)[1]);
                        const std::size_t syMin = static_cast<std::size_t>(std::max(dify, signedsize(0)));
                        const std::size_t syMax = (static_cast<signedsize>(dims[3]) + dify) < 0 ? 0 : ((dims[3] + dify) > std::get<4>(attrs)[1] ? std::get<4>(attrs)[1] : dims[3] + dify);
                        const std::size_t oIndexFull = oIndex + ox*oySize + oy;
                        const signedsize ix = static_cast<signedsize>(ox * std::get<0>(attrs)[0]);
                        const signedsize iy = static_cast<signedsize>(oy * std::get<0>(attrs)[1]);

                        if (sxMin == 0 && syMin == 0 && sxMax == 3 && syMax == 3) {
                            output[oIndexFull] += (weights[wIndex + 0*std::get<4>(attrs)[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+0)*dims[3] + static_cast<std::size_t>(iy+0)] +
                                                   weights[wIndex + 0*std::get<4>(attrs)[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+0)*dims[3] + static_cast<std::size_t>(iy+1)] +
                                                   weights[wIndex + 0*std::get<4>(attrs)[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+0)*dims[3] + static_cast<std::size_t>(iy+2)] +
                                                   weights[wIndex + 1*std::get<4>(attrs)[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+1)*dims[3] + static_cast<std::size_t>(iy+0)] +
                                                   weights[wIndex + 1*std::get<4>(attrs)[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+1)*dims[3] + static_cast<std::size_t>(iy+1)] +
                                                   weights[wIndex + 1*std::get<4>(attrs)[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+1)*dims[3] + static_cast<std::size_t>(iy+2)] +
                                                   weights[wIndex + 2*std::get<4>(attrs)[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+2)*dims[3] + static_cast<std::size_t>(iy+0)] +
                                                   weights[wIndex + 2*std::get<4>(attrs)[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+2)*dims[3] + static_cast<std::size_t>(iy+1)] +
                                                   weights[wIndex + 2*std::get<4>(attrs)[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+2)*dims[3] + static_cast<std::size_t>(iy+2)]);
                        } else {
                            for (std::size_t sx = sxMin; sx < sxMax; ++sx) {
                                for (std::size_t sy = syMin; sy < syMax; ++sy) {
                                    output[oIndexFull] += weights[wIndex + sx*std::get<4>(attrs)[1] + sy] *
                                                            input[iIndex + static_cast<std::size_t>(ix+static_cast<signedsize>(sx))*dims[3] + static_cast<std::size_t>(iy+static_cast<signedsize>(sy))];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

namespace {
static Registrar<ConvImpl2DForward_cpu> registrarConvImpl2DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::ConvImpl2D_cpu_forward_kernel<float, float, float, float>);
static Registrar<ConvImpl2DForward_cpu> registrarConvImpl2DForward_cpu_Float16(
        {DataType::Float16, DataType::Float16, DataType::Float16, DataType::Float16},
        Aidge::ConvImpl2D_cpu_forward_kernel<half_float::half, half_float::half, half_float::half, half_float::half>);
static Registrar<ConvImpl2DForward_cpu> registrarConvImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::ConvImpl2D_cpu_forward_kernel<int, int, int, int>);
static Registrar<ConvImpl2DForward_cpu> registrarConvImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::ConvImpl2D_cpu_forward_kernel<double, double, double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CONVIMPL_FORWARD_KERNEL_H_ */
