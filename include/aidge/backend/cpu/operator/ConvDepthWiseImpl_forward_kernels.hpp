/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/ConvDepthWiseImpl.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <cmath>
#include <cstddef>
#include <array>
#include <algorithm>

namespace Aidge {
/**
 * @brief Forward kernel for 2D ConvDepthWiseolution on CPU backend.
 * @tparam I Input data type.
 * @tparam W Weight data type.
 * @tparam B Bias data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param weights_ const weight Tensor.
 * @param biases_ const Biais Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class W, class B, class O>
void ConvDepthWiseImpl2D_cpu_forward_kernel(const ConvDepthWise_Op<2>::Attrs &attrs, const std::array<DimSize_t, 4> &dims,
                                       const void *input_, const void *weights_, const void *biases_, void *output_) {
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    const W *weights = static_cast<const W *>(weights_);
    const B *biases = static_cast<const B *>(biases_);
    O *output = static_cast<O *>(output_);


    // output H size
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[2] - std::get<3>(attrs)[0] + std::get<0>(attrs)[0]) /
                                static_cast<float>(std::get<0>(attrs)[0])));
    // output W size
    const std::size_t oySize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[3] - std::get<3>(attrs)[1] + std::get<0>(attrs)[1]) /
                                static_cast<float>(std::get<0>(attrs)[1])));

    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, ch, Xin, Yin)
    // weight (outCh, ch, kernelX, kernelY)
    // does not take Dilation attribute into account
    using signedsize = std::make_signed<std::size_t>::type;
    for (std::size_t batch = 0; batch < dims[0]; ++batch) {
        for (std::size_t ch = 0; ch < std::get<2>(attrs); ++ch) {
            const std::size_t oIndex = (ch + batch*std::get<2>(attrs)) * oxSize * oySize;
            B biasVal = ((!std::get<4>(attrs)) && biases != nullptr) ? biases[ch] : B(0);
            std::fill(output + oIndex, output+(oIndex+oxSize*oySize), biasVal);
            const std::size_t iIndex = (ch + batch*dims[1]) * dims[2] * dims[3];
            const std::size_t wIndex = ch * std::get<3>(attrs)[0] * std::get<3>(attrs)[1];
            for (std::size_t ox = 0; ox < oxSize; ++ox) {
                const signedsize difx = static_cast<signedsize>(- ox * std::get<0>(attrs)[0]);
                const std::size_t sxMin = static_cast<std::size_t>(std::max(difx, signedsize(0)));
                const std::size_t sxMax = (static_cast<signedsize>(dims[2]) + difx) < 0 ? 0 : ((dims[2] + difx) > std::get<3>(attrs)[0] ? std::get<3>(attrs)[0] : dims[2] + difx);
                for (std::size_t oy = 0; oy < oySize; ++oy) {
                    const signedsize dify = static_cast<signedsize>(- oy * std::get<0>(attrs)[1]);
                    const std::size_t syMin = static_cast<std::size_t>(std::max(dify, signedsize(0)));
                    const std::size_t syMax = (static_cast<signedsize>(dims[3]) + dify) < 0 ? 0 : ((dims[3] + dify) > std::get<3>(attrs)[1] ? std::get<3>(attrs)[1] : dims[3] + dify);
                    const std::size_t oIndexFull = oIndex + ox*oySize + oy;
                    const signedsize ix = static_cast<signedsize>(ox * std::get<0>(attrs)[0]);
                    const signedsize iy = static_cast<signedsize>(oy * std::get<0>(attrs)[1]);

                    if (sxMin == 0 && syMin == 0 && sxMax == 3 && syMax == 3) {
                        output[oIndexFull] +=  (weights[wIndex + 0*std::get<3>(attrs)[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+0)*dims[3] + static_cast<std::size_t>(iy+0)] +
                                                weights[wIndex + 0*std::get<3>(attrs)[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+0)*dims[3] + static_cast<std::size_t>(iy+1)] +
                                                weights[wIndex + 0*std::get<3>(attrs)[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+0)*dims[3] + static_cast<std::size_t>(iy+2)] +
                                                weights[wIndex + 1*std::get<3>(attrs)[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+1)*dims[3] + static_cast<std::size_t>(iy+0)] +
                                                weights[wIndex + 1*std::get<3>(attrs)[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+1)*dims[3] + static_cast<std::size_t>(iy+1)] +
                                                weights[wIndex + 1*std::get<3>(attrs)[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+1)*dims[3] + static_cast<std::size_t>(iy+2)] +
                                                weights[wIndex + 2*std::get<3>(attrs)[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+2)*dims[3] + static_cast<std::size_t>(iy+0)] +
                                                weights[wIndex + 2*std::get<3>(attrs)[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+2)*dims[3] + static_cast<std::size_t>(iy+1)] +
                                                weights[wIndex + 2*std::get<3>(attrs)[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+2)*dims[3] + static_cast<std::size_t>(iy+2)]);
                    } else {
                        for (std::size_t sx = sxMin; sx < sxMax; ++sx) {
                            for (std::size_t sy = syMin; sy < syMax; ++sy) {
                                output[oIndexFull] += weights[wIndex + sx*std::get<3>(attrs)[1] + sy] *
                                                        input[iIndex + static_cast<std::size_t>(ix+static_cast<signedsize>(sx))*dims[3] + static_cast<std::size_t>(iy+static_cast<signedsize>(sy))];
                            }
                        }
                    }
                }
            }
        }
    }
}

namespace {
static Registrar<ConvDepthWiseImpl2DForward_cpu> registrarConvDepthWiseImpl2DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<float, float, float, float>);
static Registrar<ConvDepthWiseImpl2DForward_cpu> registrarConvDepthWiseImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<int, int, int, int>);
static Registrar<ConvDepthWiseImpl2DForward_cpu> registrarConvDepthWiseImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<double, double, double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_FORWARD_KERNEL_H_ */
