/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SOFTMAXIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_SOFTMAXIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"
#include <cstddef>
#include <cmath>
#include "aidge/data/Data.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/SoftmaxImpl.hpp"

namespace Aidge {
template <class I, class O>
void SoftmaxImpl_cpu_forward_kernel(std::size_t axisIdx, const std::vector<DimSize_t>& inputDims, const void* input_, void* output_)
{
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    std::size_t postAxisElems = 1;
    for (std::size_t i = axisIdx + 1; i < inputDims.size(); ++i) {
        postAxisElems *= inputDims[i];
    }
    std::size_t preAxisElems = 1;
    for (std::size_t i = 0; i < axisIdx; ++i) {
        preAxisElems *= inputDims[i];
    }

    for (std::size_t i = 0; i < preAxisElems; ++i) {
        for (std::size_t j = 0; j < postAxisElems; ++j) {
            // Calculate sum of exponentials within the axis
            I sumExp = 0;
            for (std::size_t k = 0; k < inputDims[axisIdx]; ++k) {
                std::size_t inIdx = i * inputDims[axisIdx] * postAxisElems + k * postAxisElems + j;
                sumExp += std::exp(input[inIdx]);
            }

            // Calculate softmax for the current slice along the axis
            for (std::size_t  k = 0; k < inputDims[axisIdx]; ++k) {
                std::size_t inIdx = i * inputDims[axisIdx] * postAxisElems + k * postAxisElems + j;
                output[inIdx] = std::exp(input[inIdx]) / sumExp;
            }
        }
    }
}

namespace {
static Registrar<SoftmaxImplForward_cpu> registrarSoftmaxImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::SoftmaxImpl_cpu_forward_kernel<float, float>);
static Registrar<SoftmaxImplForward_cpu> registrarSoftmaxImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32}, Aidge::SoftmaxImpl_cpu_forward_kernel<int, int>);
static Registrar<SoftmaxImplForward_cpu> registrarSoftmaxImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::SoftmaxImpl_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SOFTMAXIMPL_FORWARD_KERNEL_H_ */
