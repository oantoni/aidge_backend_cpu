/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_PADIMPL_H_
#define AIDGE_CPU_OPERATOR_PADIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// class Pad_Op;

// compute kernel registry for forward and backward
class PadImpl2DForward_cpu
    : public Registrable<PadImpl2DForward_cpu,
                         std::tuple<DataType, DataType>,
                         void(const Pad_Op<2>::Attrs &, const std::array<DimSize_t, 4> &, const void *,
                              void *)> {};
class PadImpl2DBackward_cpu
    : public Registrable<PadImpl2DBackward_cpu,
                         std::tuple<DataType, DataType>,
                         void(const Pad_Op<2>::Attrs &, const std::array<DimSize_t, 4> &, const void *,
                              void *)> {};

class PadImpl2D_cpu : public OperatorImpl {
public:
    PadImpl2D_cpu(const Pad_Op<2> &op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<PadImpl2D_cpu> create(const Pad_Op<2> &op) {
        return std::make_unique<PadImpl2D_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
// add cpu backend to Pad_Op<2> implementation registry
static Registrar<Pad_Op<2>> registrarPadImpl2D_cpu("cpu", Aidge::PadImpl2D_cpu::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_PADIMPL_H_ */
