/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_ADDIMPL_H_
#define AIDGE_CPU_OPERATOR_ADDIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>   // std::unique_ptr, std::make_unique
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

// compute kernel registry for forward and backward
class AddImplForward_cpu
    : public Registrable<AddImplForward_cpu, std::tuple<DataType, DataType>, void(const std::vector<const void*>, const std::vector<std::vector<std::size_t>>&, const std::size_t, const std::vector<std::size_t>&, void*)> {};

class AddImplBackward_cpu
    : public Registrable<AddImplBackward_cpu, std::tuple<DataType, DataType>, void(const std::vector<const void*>, const std::vector<std::vector<std::size_t>>&, const std::size_t, const std::vector<std::size_t>&, void*)> {};


class AddImpl_cpu : public OperatorImpl {
public:
    AddImpl_cpu(const Add_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<AddImpl_cpu> create(const Add_Op& op) {
        return std::make_unique<AddImpl_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t /*inputIdx*/) const override final;
    void forward() override;
};

namespace {
static Registrar<Add_Op> registrarAddImpl_cpu("cpu", Aidge::AddImpl_cpu::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_ADDIMPL_H_ */
