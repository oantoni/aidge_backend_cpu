/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_DIVIMPL_H_
#define AIDGE_CPU_OPERATOR_DIVIMPL_H_

#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Div.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

// compute kernel registry for forward and backward
class DivImplForward_cpu
    // : public Registrable<DivImplForward_cpu, std::tuple<DataType, DataType, DataType>, void(const std::vector<std::size_t>&, const std::vector<std::size_t>&, const std::vector<std::size_t>&, const void*, const void*,void*)> {
    : public Registrable<DivImplForward_cpu, std::tuple<DataType, DataType, DataType>, void(const std::size_t, const std::size_t, const std::size_t, const void*, const void*,void*)> {
};
class DivImplBackward_cpu
    : public Registrable<DivImplBackward_cpu, std::tuple<DataType, DataType, DataType>, void(const std::vector<std::size_t>&, const std::vector<std::size_t>&, const std::vector<std::size_t>&, const void*, const void*, void*)> {
};

class DivImpl_cpu : public OperatorImpl {
public:
    DivImpl_cpu(const Div_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<DivImpl_cpu> create(const Div_Op& op) {
        return std::make_unique<DivImpl_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;

    void forward() override final;
};

namespace {
static Registrar<Div_Op> registrarDivImpl_cpu("cpu", Aidge::DivImpl_cpu::create);
}
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_DIVIMPL_H_ */
