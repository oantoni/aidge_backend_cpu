/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_DATA_BROADCASTING_H_
#define AIDGE_CPU_DATA_BROADCASTING_H_

#include <vector>

namespace Aidge {

// Function to broadCast an input dims vector into the same size as an outputDims vector

    /**
     * @brief  Broadcast an input dims vector into the same size as an outputDims vector
     * @details The missing dimensions would be completed by 1
     * @param outputDims The vector of dimensions to follow 
     * @param dimsToBroadcast The vecotr of dimensions to braodcast
     * @return std::vector<std::size_t> a broadcasted vector by addding 1 on the missing dimensions.
     */
    std::vector<std::size_t> getBroadcastedDims(const std::vector<std::size_t>& outputDims, const std::vector<std::size_t>& dimsToBroadcast);

    /**
     * @brief Get a vector of indexes along the dimensions vector from a flattened index
     * @param dimensions The vector of dimensions we want the indexes on
     * @param idx The flattened index
     * @return std::vector<std::size_t> vector of indexes along dimensions.
     */
    std::vector<std::size_t> getMultiDimIndices(const std::vector<std::size_t>& dimensions, std::size_t idx);

    // Function to get a flattened index from multi-dimensional indices
    /**
     * @brief Get a flattened index the dimensions vector from a given vector of indices on a broadcasted vector
     * @param dimensions The vector of dimensions we want the flattened index on
     * @param indices The vector of indices we want to flatten
     * @return std::size_t The flattened index on the dimensions vector
     */
    std::size_t getFlattenedIndex(const std::vector<std::size_t>& dimensions, const std::vector<std::size_t>& indices);

} // namespace Aidge

#endif // AIDGE_CPU_DATA_BROADCASTING_H_