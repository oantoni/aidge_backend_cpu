#include <pybind11/pybind11.h>
#include "aidge/utils/sys_info/CpuVersionInfo.hpp"

namespace py = pybind11;
namespace Aidge {
void init_cpu_sys_info(py::module& m){
    m.def("show_cpu_version", &showCpuVersion);
}
}
