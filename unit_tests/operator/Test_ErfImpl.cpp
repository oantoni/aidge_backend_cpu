/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Erf.hpp"

#include "aidge/backend/cpu.hpp"

#include <memory>


using namespace Aidge;

TEST_CASE("[cpu/operator] Erf(forward)") {
    SECTION("1D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array1D<float,10> {
            {0.41384590, 0.43120754, 0.93762982, 0.31049860, 0.77547199, 0.09514862,
              0.16145366, 0.42776686, 0.43487436, 0.41170865}
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array1D<float,10> {
                {0.44163144, 0.45801866, 0.81516320, 0.33941913, 0.72722000, 0.10704061,
              0.18061027, 0.45479023, 0.46144873, 0.43959764}
        });

        std::shared_ptr<Node> myErf = Erf();
        auto op = std::static_pointer_cast<OperatorTensor>(myErf -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cpu");
        myErf->forward();

        float* resPtr = static_cast<float*>(op->getOutput(0)->getImpl()->rawPtr());
        float* expectedPtr = static_cast<float*>(expectedOutput->getImpl()->rawPtr());
        for (std::size_t i = 0; i< expectedOutput->size(); ++i) {
            REQUIRE(std::abs(resPtr[i]-expectedPtr[i]) < 0.00001);
        }
    }

    SECTION("3D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array3D<float,2,2,3> {
            {
                {
                    {0.97037154, 0.86208081, 0.77767169},
                    {0.38160080, 0.11422747, 0.77284443},
                },
                {
                    {0.51592529, 0.72543722, 0.54641193},
                    {0.93866944, 0.97767913, 0.34172094}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array3D<float,2,2,3> {
            {
                {
                    {0.83003384, 0.77721894, 0.72857803},
                    {0.41057193, 0.12833349, 0.72559172},
                },
                {
                    {0.53438270, 0.69507217, 0.56032562},
                    {0.81564975, 0.83322692, 0.37109339}
                }
            }
        });

        std::shared_ptr<Node> myErf = Erf();
        auto op = std::static_pointer_cast<OperatorTensor>(myErf -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cpu");
        myErf->forward();

        float* resPtr = static_cast<float*>(op->getOutput(0)->getImpl()->rawPtr());
        float* expectedPtr = static_cast<float*>(expectedOutput->getImpl()->rawPtr());
        for (std::size_t i = 0; i< expectedOutput->size(); ++i) {
            REQUIRE(std::abs(resPtr[i]-expectedPtr[i]) < 0.00001);
        }
    }
}